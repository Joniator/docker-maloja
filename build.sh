#!/usr/bin/env bash
set -e

function docker_tag_exists() {
  curl --silent -f -lSL https://index.docker.io/v1/repositories/$1/tags/$2 > /dev/null
}

if [ -z "$1" ]; then
  RELEASE_TAG=$(curl https://pypi.org/pypi/malojaserver/json | jq ".info.version" | cut -d"\"" -f2)
else
  RELEASE_TAG="$1"
fi
VERSION="v$RELEASE_TAG"
echo "Building version $VERSION"

if docker_tag_exists joniator/maloja $VERSION; then
  echo "joniator/maloja:$VERSION exists, exiting without building"
  exit 0
else
  echo "joniator/maloja:$VERSION does not exist, building"
  DOCKER_LATEST_TAG="joniator/maloja:latest"
  DOCKER_VERSION_TAG="joniator/maloja:$VERSION"
  
  docker build --pull --build-arg MALOJA_RELEASE=$RELEASE_TAG -t "$DOCKER_LATEST_TAG" -t "$DOCKER_VERSION_TAG" .
  docker push "$DOCKER_LATEST_TAG"
  docker push "$DOCKER_VERSION_TAG"
fi
